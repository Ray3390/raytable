
# raytable
前端table插件，很多思路都是参照layuitable实现 :smile:  也封装了一版layui-table2，[可参见](https://gitee.com/Ray3390/table2)



### 先看几张效果图

![07](https://gitee.com/uploads/images/2018/0109/163654_93fa993c_1428435.png "07.png")
![01](https://gitee.com/uploads/images/2018/0108/093914_0031c46f_1428435.png "01.png")
![02](https://gitee.com/uploads/images/2018/0108/094031_9179f663_1428435.png "02.png")
![03](https://gitee.com/uploads/images/2018/0108/094042_198fd81f_1428435.png "03.png")
![04](https://gitee.com/uploads/images/2018/0108/094211_4e226f28_1428435.png "04.png")
![05](https://gitee.com/uploads/images/2018/0108/094217_acae8cbe_1428435.png "05.png")
![06](https://gitee.com/uploads/images/2018/0108/094224_408956cd_1428435.png "06.png")


### 兼容性
因本人经验不足，无法兼容所有浏览器，理论上支持H5的浏览器应该都可以（见谅）。
在IE8（H5的效果无效）、IE11、Edge、Chrome62、firefox57、360浏览器8.1（内核45）内测试均正常，其他浏览器没装就没再查看。



### 参数说明，最终以代码为准
<pre>

v1.3 2018-3-14
1、cols增加dataType属性，设为html时避免执行脚本
2、修改最后一列是否去掉右边框的算法

v1.2 2018-2-5
1、常规更新
2、合并check、uncheck方法


raytable.js使用说明：
表格支持定宽或自适应宽度，
使用方法 var tableIns = $("#table").raytable(option);

table属性option：
url：ajax请求路径
method：ajax请求方法，可选get|post，默认get
width：容器宽度，数值
height：容器高度，数值|full-数值|sel-jqselectors-数值(jqselectors选择器不支持-符号，因为需要按照-分割)。
             使用sel模式时，只需填写table下方的容器选择器，系统会自动剔除上方的dom高度，如果固定数值没有，请填写0，不能省略
minHeight： 表格最小高度，默认80
skin：皮肤，可选col|row|none，col：列边框风格，row：行边框风格，none：无边框风格
even：隔行换色，可选true|false，默认false
expandRow: 行是否允许展开，可选true|false，默认false。需要配合on("expand", function(){})使用
singleSelect：单选行，可选true|false，默认false，可以选中多行
nowrap：数据是否自动换行，默认true，不自动换行，填写true性能更高一下
localSort： 本地排序，默认true，不想使用本地排序，请配置成false；如果由后台排序建议配置成false
loading： 请求数据时，是否显示loading，默认true
cellMinWidth： 所有单元格默认最小宽度，默认60px
serialField：主键唯一字段，建议填写，否则删除数据的时候性能减弱，默认id，区分大小写
colspanDefs：数组，合并单元格，例如：["Name", "Body"]将会按照Name和Body进行单元格合并
page：是否分页，默认false，可选true，或则如下
    {
        align：对齐方式，可选left|right|center，默认left
        curr：当前页
        limit：每页显示的数量，默认10条
        limits：每页条数选择，默认[10, 20, 30, 40, 50, 60, 70, 80, 90]
    }

属性事件：
onAjaxBeforeSend(xhr)：和ajax beforeSend参数一致，此时this为本次ajax请求时传递的options参数。
                                        如果返回false可以取消本次ajax请求
onAjaxSuccess(result)：ajax成功请求数据时（table清除上次数据后），在这里可以做数据统一。
                                    需要返回json数据，格式为{code: 0, msg: '错误信息', count: 100, data: []},code为0时表示成功。
                                    此时this为本次ajax请求时传递的options参数
onAjaxError(xhr, textStatus, errorThrown)：和ajax error参数一致，此时this为本次ajax请求时传递的options参数

onBeforeAddRow(rowindex, data)： 添加行数据前返回行索引和行数据
onAddRow(rowindex, data)： 添加行数据后返回行索引和行数据
onPageLimitChanged(preLimit, toLimit)：当每页条数发生变化时触发，preLimit为之前的页条数，toLimit为修改后条数
onPageJump(prePage, toPage)：页码跳转，进行数据请求绑定前，prePage为之前的页码，toPage为修改后的页码
onComplete(data)：所有处理完毕（table渲染数据结束），data为onAjaxSuccess返回的result数据



cols：属性
filed：绑定字段，需要显示的列必填，主标题副标题可不填
title：标题
sort：排序，可选true|false，默认false
width：宽度，可以是定宽，百分比；不填则自适应宽度
rowspan：行合并
colspan：列合并
resize：拖拽宽度，默认false
align：对齐方式
style：设置head样式，支持css所有样式，带小横线的需要用引号包起来，eg：style: { "font-size": "14px" }
dataStyle: 设定数据样式，只有包含field时才有效，带小横线的需要用引号包起来，eg：style: { "font-size": "14px" }
dataType: 数据格式，可选html|非html值。当为html时，会进行转码，其他值不做处理，有formatter时无效，自行处理
formatter(data, index)：数据格式化，传入一个函数比如操作列，data当前行数据，index行号从0开始


tableIns方法：
reload：重新加载
getSelectedRows：获取选中行，返回数组，数据格式每个包含一个index，data，index为选中行序号，
                             从0开始，data为当前行数据，单选时注意使用list[0]
getData：获取table所有数据
on(event, callback)：事件绑定。event字符串，可选如下，callback为函数
    tool返回函数function(evt, obj)，evt为事件，obj包含index，data，tr，del，update，index为tr行号，
                                                     tr为dom对象，data为数据（只监听包含event的click事件）
    sort返回函数(initSort)，initSort对象包含：sortField排序字段，sortType排序方式，可选asc|desc
    expand返回函数(rowindex, data, container): 行展开时回调，rowindex：行数据前返回行索引，data行数据，
                                                                container为容器，需要返回一个字符串
    check返回函数(index, row)：行选中，index为tr行号，row为tr（jquery对象）
    uncheck返回函数(index, row)：行取消选中，index为tr行号，row为tr（jquery对象）



可全局配置参数：
$.rayTableOptions：属性包含如下
        expandRow: false,
        nowrap: true,
        minHeight: 80,
        singleSelect: false,
        localSort: true,
        loading: true,
        cellMinWidth: 60,
        serialField: "id"
    

注意：
1、进行拖拽后，表格将不再自适应宽度
2、cols中同时设置style和dataStyle时align无效。并且不建议设置max-width，首先对td无效，再者禁止换行时数据和表头有1px之差
3、建议所有列书写一个minWidth，因为当数据允许换行时(nowrap=true时)，表头可能会被换行，不好看
4、

</pre>



自己空闲时搞了这么一个table，很多地方参见了layuitable，很高兴认识了layui，希望layui越来越好(^-^)

